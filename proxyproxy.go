// Good Reference: https://medium.com/@mlowicki/http-s-proxy-in-golang-in-less-than-100-lines-of-code-6a51c2f2c38c
//
package main

import (
	"bufio"
	"crypto/tls"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"strings"
	"syscall"
	"time"

	"golang.org/x/crypto/ssh/terminal"
)

var version string
var build string

var exPath string

var config configFile
var proxyCreds proxyCredsT

var asciiArt = `
 ______  ______  ______  ______  ______  ______  ______  ______   
|______||______||______||______||______||______||______||______|   
______                        ______                        
| ___ \                       | ___ \                       
| |_/ /_ __  ___ __  __ _   _ | |_/ /_ __  ___ __  __ _   _ 
|  __/| '__|/ _ \\ \/ /| | | ||  __/| '__|/ _ \\ \/ /| | | |
| |   | |  | (_) |>  < | |_| || |   | |  | (_) |>  < | |_| |
\_|   |_|   \___//_/\_\ \__, |\_|   |_|   \___//_/\_\ \__, |
                         __/ |                         __/ |
                        |___/                         |___/ 
 ______  ______  ______  ______  ______  ______  ______  ______   
|______||______||______||______||______||______||______||______|   
`

var requestMethodHandlers map[string]func(http.ResponseWriter, *http.Request)

type proxyCredsT struct {
	User string
	Pass string
}

type configFile struct {
	Port string
	Dest string
}

func promptString(promptString string) string {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print(promptString)
	result, _ := reader.ReadString('\n')

	return strings.TrimSpace(result)
}

func promptPasswd(promptString string) string {
	fmt.Print(promptString)
	bytePassword, err := terminal.ReadPassword(int(syscall.Stdin))
	if err != nil {
		fmt.Fprintln(os.Stderr, "\nError collecting Password: ", err, "Try using a TTY terminal?")
		os.Exit(1)
	}
	return strings.TrimSpace(string(bytePassword))
}

func parseConfig() {
	var filename = "proxyproxy.config.json"
	var filepath = filename

	if _, err := os.Stat(filepath); os.IsNotExist(err) {
		filepath = path.Join(exPath, filename)
	}

	dat, err := ioutil.ReadFile(filepath)
	if err != nil {
		fmt.Print("Config File Missing! [", filename, "]\n\n")
		fmt.Print("Prompting for new config...\n\n")

		promptConfig()

		// Write config values to file
		b, err := json.Marshal(config)
		if err == nil {
			err := ioutil.WriteFile(filename, b, 0644)
			if err != nil {
				fmt.Print("Error occured writing config [", filepath, "]:", err)
			}
		}

		fmt.Println("")
	}

	// fmt.Println(string(dat))
	err = json.Unmarshal(dat, &config)
}

func promptConfig() {
	result := promptString("ProxyProxy LISTEN port? [8888] ")
	if len(result) <= 0 {
		result = "8888"
	}
	config.Port = result

	result = promptString("Destination Proxy Address? [proxy.example.com:8080]: ")
	if len(result) <= 0 {
		result = "proxy.example.com:8080"
	}

	config.Dest = result
}

func copyHeader(dst, src http.Header) {
	for k, vv := range src {
		for _, v := range vv {
			dst.Add(k, v)
		}
	}
}

func createBasicAuthHeaderValue() string {
	auth := proxyCreds.User + ":" + proxyCreds.Pass
	basicAuth := "Basic " + base64.StdEncoding.EncodeToString([]byte(auth))

	return basicAuth
}

func requestGenericHandler(w http.ResponseWriter, r *http.Request) {
	proxyURL, err := url.Parse("http://" + config.Dest)
	transport := &http.Transport{Proxy: http.ProxyURL(proxyURL)}

	basicAuth := createBasicAuthHeaderValue()
	r.Header.Add("Proxy-Authorization", basicAuth)

	resp, err := transport.RoundTrip(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusServiceUnavailable)
		return
	}

	defer resp.Body.Close()
	copyHeader(w.Header(), resp.Header)
	w.WriteHeader(resp.StatusCode)
	io.Copy(w, resp.Body)
}

func requestConnectHandler(w http.ResponseWriter, r *http.Request) {
	raw, err := httputil.DumpRequest(r, false)

	basicAuth := createBasicAuthHeaderValue()
	newraw := string(raw[:len(raw)-2]) + "Proxy-Authorization: " + basicAuth + "\r\n\r\n"

	destConn, err := net.DialTimeout("tcp", config.Dest, 10*time.Second)

	if err != nil {
		http.Error(w, err.Error(), http.StatusServiceUnavailable)
		fmt.Println(err)
		return
	}

	destConn.Write([]byte(newraw))

	hijacker, ok := w.(http.Hijacker)

	if !ok {
		http.Error(w, "Hijacking not supported", http.StatusInternalServerError)
		fmt.Println(ok)
		return
	}
	clientConn, _, err := hijacker.Hijack()
	if err != nil {
		http.Error(w, err.Error(), http.StatusServiceUnavailable)
		fmt.Println(err)
	}
	go transfer(destConn, clientConn)
	go transfer(clientConn, destConn)
}

func transfer(destination io.WriteCloser, source io.ReadCloser) {

	defer destination.Close()
	defer source.Close()
	io.Copy(destination, source)
}

func requestHandler(w http.ResponseWriter, r *http.Request) {

	sig := fmt.Sprintf("[%s] %s", r.Method, r.URL)

	defer timeTrack(time.Now(), sig)
	log.Printf(sig)

	handler, ok := requestMethodHandlers[r.Method]
	if !ok {
		handler = requestGenericHandler
	}

	handler(w, r)
}

func timeTrack(start time.Time, name string) {
	elapsed := time.Since(start)
	log.Printf("%s took %s", name, elapsed)
}

func startServer() {
	server := &http.Server{
		Addr:         "127.0.0.1:" + config.Port,
		Handler:      http.HandlerFunc(requestHandler),
		TLSNextProto: make(map[string]func(*http.Server, *tls.Conn, http.Handler)),
	}

	fmt.Printf("\nListening on [%s] forwarding to [%s]...\n", server.Addr, config.Dest)

	log.Fatal(server.ListenAndServe())
}

func getProxyCreds() {
	fmt.Printf("Dest Proxy: %s\n\n", config.Dest)
	proxyCreds.User = promptString("Dest Proxy Username: ")
	proxyCreds.Pass = promptPasswd("Dest Proxy Password: ")
	fmt.Printf("\n")
}

func init() {
	// Print ASCII Art, author, and version
	fmt.Println(" " + strings.TrimSpace(asciiArt))
	ver := fmt.Sprintf("v%s.%s", version, build)
	fmt.Printf("By David Caravello %45s\n\n", ver)

	// Setup handlers for http methods
	requestMethodHandlers = make(map[string]func(http.ResponseWriter, *http.Request))
	requestMethodHandlers[http.MethodConnect] = requestConnectHandler
	// requestMethodHandlers[http.MethodGet] = requestGetHandler

	// Get Executables Path
	ex, err := os.Executable()
	if err != nil {
		panic(err)
	}
	exPath = filepath.Dir(ex)

	parseConfig()
	getProxyCreds()
}

func main() {
	startServer()
}
